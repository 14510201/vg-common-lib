CXX = g++
CXXFLAGS = -Iinclude
LDFLAGS = -lboost_program_options -lredis++ -lhiredis -pthread

# Define the source and object files
SOURCES = $(wildcard *.cpp)
OBJ_DIR = ../build
OBJECTS = $(patsubst %.cpp, $(OBJ_DIR)/%.o, $(SOURCES))
LIB_DIR = ../lib

# Define the static lib file
REDIS_LIB = $(LIB_DIR)/libRedisClient.a

all: $(REDIS_LIB) 

# Rule for building object files
$(OBJ_DIR)/%.o: %.cpp
	mkdir -p $(OBJ_DIR)
	$(CXX) $(CXXFLAGS) -DBOOST_ALLOW_DEPRECATED_HEADERS  -c $< -o $@

$(REDIS_LIB): $(OBJECTS)
	mkdir -p $(LIB_DIR)
	ar rcs $(REDIS_LIB) $(OBJECTS)

clean:
	rm -f $(OBJECTS) $(REDIS_LIB)

.PHONY: all clean

