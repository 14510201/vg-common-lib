// RedisQueueRepository.h
#pragma once

#include <sw/redis++/redis++.h>

class RedisSubscriberRepository  {
private:
    sw::redis::Redis redisclient;

public:
    RedisSubscriberRepository(const std::string& host, int port, sw::redis::ConnectionOptions& connoptions);
    sw::redis::Subscriber getSubscriber(const std::string& host, int port);
};


