
#pragma once

#include <string>

/// <summary>
/// 	Class: PspOobPacket. Parse a PCAPNG file and extract the PSP OOB payload.
/// </summary>
class PspOobPacket
{
public:

	/// <summary>	Structure to define a sequence gap. </summary>
	struct SequenceGap_t
	{
		SequenceGap_t(int packetLocation, uint16_t seqNumber, uint16_t gap) :
			packetLocation(packetLocation), seqNumber(seqNumber), gap(gap) {}
		int					packetLocation;
		uint16_t			seqNumber;
		uint16_t			gap;
	};


protected:
	const int					PcapngSectionHeaderBlockType = 0x0a0d0d0a;
	const int					PcapngByteOrderMagicLittleEndian = 0x1a2b3c4d;
	const int					PcapngPacketBlockType = 6;
	const uint8_t				L2TpProtocolId = 0x73;
	const int					MaxIqFrameWords = 512;
	const int					PacketHeaderBytes = 38;
	const int					HeaderOffsetProtocolType = 23;
	const int					HeaderOffsetSrcIp = 26;
	const int					HeaderOffsetDestIp = 30;

	std::FILE*					pFile;
	uint32_t					srcIp;
	uint32_t					destIp;
	uint8_t						protocolId;
	uint32_t*					pWorkingBufr;
	bool						machineIsLittleEndian;
	bool						fileIsLittleEndian;
	std::vector<SequenceGap_t>	seqGaps;
	uint16_t					lastSeqNumber;
	int							pktLoc;


public:
	PspOobPacket(void) = delete;
	PspOobPacket( std::string srcIpStr, std::string destIpStr);
	~PspOobPacket(void);

	PspOobPacket&				operator += (const int inc);
	std::vector<uint32_t>		operator () (void);

	bool						SequenceGapsDetected(void) { return (seqGaps.size() != 0); }
	std::vector<SequenceGap_t>& SequenceGaps(void) { return seqGaps; }

protected:
	bool						FilterMatchPacket(uint32_t* pRemainingCapturedPktBytes, int* pRemainingBlockBytes);
	std::vector<uint32_t>		GetNextPacket(void);
	void						SkipPackets(int count);
	void						ParseSectionHeaderBlock(void);
	inline void					CorrectPcapngFormatEndianness(uint32_t* pValue);
	inline void					CorrectPcapngFormatEndianness(uint32_t* pValues, int count);
	inline void					CorrectPspOobEndianness(uint32_t* pValue);
	inline void					CorrectPspOobEndianness(uint32_t* pValues, int count);
};
