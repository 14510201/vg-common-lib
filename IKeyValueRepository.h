// IKeyValueRepository.h
#pragma once

#include <string>

class IKeyValueRepository {
public:
    virtual void set(const std::string& key, const std::string& value) = 0;
    virtual std::string get(const std::string& key) = 0;
    virtual ~IKeyValueRepository() = default;
};


