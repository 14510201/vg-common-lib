// RedisKeyValueRepository.cpp
#include "RedisKeyValueRepository.h"

RedisKeyValueRepository::RedisKeyValueRepository(const std::string& host, int port)
    : redisclient("tcp://" + host + ":" + std::to_string(port)) {}

void RedisKeyValueRepository::set(const std::string& key, const std::string& value) {
    redisclient.set(key, value);
}

std::string RedisKeyValueRepository::get(const std::string& key) {
    auto val = redisclient.get(key);
    if (val) {
        return *val;
    }
    return ""; // or throw an exception if the key does not exist
}


