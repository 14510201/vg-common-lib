// IQueueRepository.h
#pragma once

#include <string>

class IQueueRepository {
public:
    virtual void enqueue(const std::string& value) = 0;
    virtual std::string dequeue() = 0;
    virtual int length() = 0;  // Added this line
    virtual ~IQueueRepository() = default;
};


