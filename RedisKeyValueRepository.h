// RedisKeyValueRepository.h
#pragma once

#include "IKeyValueRepository.h"
#include <sw/redis++/redis++.h>

class RedisKeyValueRepository : public IKeyValueRepository {
private:
    sw::redis::Redis redisclient;

public:
    RedisKeyValueRepository(const std::string& host, int port);
    void set(const std::string& key, const std::string& value) override;
    std::string get(const std::string& key) override;
};


