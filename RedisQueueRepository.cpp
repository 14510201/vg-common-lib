// RedisQueueRepository.cpp
#include "RedisQueueRepository.h"

RedisQueueRepository::RedisQueueRepository(const std::string& host, int port, const std::string& key)
    : redisclient("tcp://" + host + ":" + std::to_string(port)), queueKey(key) {}

void RedisQueueRepository::enqueue(const std::string& value) {
    redisclient.rpush(queueKey, value);
}

std::string RedisQueueRepository::dequeue() {
    auto val = redisclient.lpop(queueKey);
    if (val) {
        return *val;
    }
    return ""; // or throw an exception if you prefer
}

int RedisQueueRepository::length() {
    return redisclient.llen(queueKey);
}







