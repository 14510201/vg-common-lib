// RedisQueueRepository.h
#pragma once

#include "IQueueRepository.h"
#include <sw/redis++/redis++.h>

class RedisQueueRepository : public IQueueRepository {
private:
    sw::redis::Redis redisclient;
    std::string queueKey;

public:
    RedisQueueRepository(const std::string& host, int port, const std::string& key);
    void enqueue(const std::string& value) override;
    std::string dequeue() override;
    int length()  override;  // Added this method

    // Additional methods for set and get
    void set(const std::string& key, const std::string& value);
    std::string get(const std::string& key);
};


