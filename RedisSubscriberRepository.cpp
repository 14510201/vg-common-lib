// RedisSubscriberRepository.cpp
#include "RedisSubscriberRepository.h"


RedisSubscriberRepository::RedisSubscriberRepository(const std::string& host, int port, sw::redis::ConnectionOptions& connoptions) 
:redisclient(sw::redis::Redis(connoptions)){}


sw::redis::Subscriber RedisSubscriberRepository::getSubscriber(const std::string& host, int port) {
    // sw::redis::ConnectionOptions redisOpts;
    // redisOpts.host = host;
    // redisOpts.port = port;
    // redisclient = sw::redis::Redis(redisOpts);
    return redisclient.subscriber();
}






